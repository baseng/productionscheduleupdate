﻿Imports System.Timers
Imports System.Data.OleDb
Imports System.IO
Imports ADODB

Public Class ScheduleUpdateNew
    Private eventId As Integer = 1
    Private eventId2 As Integer = 1
    Private restartID As Integer = 1
    Private timeChk As Integer = 1
    Protected Overrides Sub OnStart(ByVal args() As String)

        Try
            ' Setup a Timer to trigger database refresh.
            Me.timer = New Timer()
            'Me.timer.Interval = 180000 ' 180s timer
            Me.timer.Interval = 300000 ' 300s timer (5 mins)
            AddHandler Me.timer.Elapsed, AddressOf Me.OnTimer
            Me.timer.Start()

            Me.DatabasePath = args(0)
            EventLog1.WriteEntry("In OnStart with path:" & Me.DatabasePath, EventLogEntryType.Warning, restartID)
            If (restartID = 65535) Then
                restartID = 1
            Else
                restartID = restartID + 1
            End If

        Catch ex As Exception
            'Write error event to log file.
            EventLog1.WriteEntry("Failure to initialize the database update service: " & ex.Message & "stack trace: " & ex.StackTrace, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If
        End Try

    End Sub

    Private Sub OnTimer(sender As Object, e As ElapsedEventArgs)
        Me.Refresh()


    End Sub

    Private Sub OnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)
        EventLog1.WriteEntry("In OnChanged")
    End Sub

    Protected Overrides Sub OnStop()
        EventLog1.WriteEntry("In OnStop", EventLogEntryType.Warning)
    End Sub

    Private Sub Refresh()
        'Refreshes the Access database using ADODB commands to update new lines
        'Calls Delete to remove unneeded records
        Dim OccurredUpdates As String

        'initialize the OccurredUpdates string
        OccurredUpdates = ""

        OccurredUpdates = Me.RefreshSchedule()
        OccurredUpdates = OccurredUpdates & ", " & Me.ScheduleQtyLogic()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshStock()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshSTNStock()
        OccurredUpdates = OccurredUpdates & ", " & Me.BuildQtyLogic()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshSystems()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshHandhelds()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshReceivers()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshEnclosures()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshMiscs()
        OccurredUpdates = OccurredUpdates & ", " & Me.RefreshOBCs()
        OccurredUpdates = OccurredUpdates & ", " & Me.CleanScheduleData()

        'Write event in log
        EventLog1.WriteEntry("Update records includes the following databases: " & OccurredUpdates, EventLogEntryType.Information)

        'check how many exceptions have occurred.  If each 10, stop the service
        If eventId >= 10 Then
            Me.Stop()
        End If

        'check how many times have entered refresh function (~30 min timer)
        If timeChk >= 10 Then
            timeChk = 1
            eventId = 1  'reset error ID every 30 minutes
        Else
            timeChk = timeChk + 1
        End If

    End Sub

    Private Function CleanScheduleData() As String
        'Removes unneeded records from the Schedule database

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim SelectRS As New ADODB.Recordset
        Dim UpdateRS As New ADODB.Recordset
        Dim InsertRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim Index As Integer
        Dim OrderKey As String
        Dim inTransBool As Boolean

        'Initial boolean
        inTransBool = False

        'Set blank as UpdateLog default
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT DISTINCT Schedule.* FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) " &
                      "LEFT JOIN ScheduleHistory ON Schedule.OrderKey2 = ScheduleHistory.OrderKey2 " &
                      "WHERE ((Schedule.OrderKey2 Not In (SELECT OrderKey2 FROM dbo_vw_ProductionAccess)) AND ((ScheduleHistory.OrderKey2) Is Null)); "
        CommandText2 = "UPDATE ScheduleHistory INNER JOIN Schedule ON Schedule.OrderKey2 = ScheduleHistory.OrderKey2 " &
                       "SET ScheduleHistory.ReqShipDate = [Schedule].[ReqShipDate], ScheduleHistory.SalesOrderLine = [Schedule].[SalesOrderLine], " &
                       "ScheduleHistory.StockCode = [Schedule].[StockCode], ScheduleHistory.SoDrawingNumber = [Schedule].[SoDrawingNumber], " &
                       "ScheduleHistory.OrderQty = [Schedule].[OrderQty], ScheduleHistory.[Handheld Built] = [Schedule].[Handheld Built],  " &
                       "ScheduleHistory.[Handheld Builder] = [Schedule].[Handheld Builder], ScheduleHistory.[Receiver Built] = [Schedule].[Receiver Built], " &
                       "ScheduleHistory.[Receiver Builder] = [Schedule].[Receiver Builder], ScheduleHistory.[Harness Built] = [Schedule].[Harness Built], " &
                       "ScheduleHistory.[Harness Builder] = [Schedule].[Harness Builder], ScheduleHistory.[Enclosure Built] = [Schedule].[Enclosure Built], " &
                       "ScheduleHistory.[Enclosure Builder] = [Schedule].[Enclosure Builder], ScheduleHistory.StrictShipDate = [Schedule].[StrictShipDate], " &
                       "ScheduleHistory.CommentsOnOneLine = [Schedule].[CommentsOnOneLine], ScheduleHistory.Country = [Schedule].[Country], " &
                       "ScheduleHistory.[QA Tech] = [Schedule].[QA Tech], ScheduleHistory.[OBC Built] = [Schedule].[OBC Built], " &
                       "ScheduleHistory.[OBC Builder] = [Schedule].[OBC Builder], ScheduleHistory.ShipToName = [Schedule].[ShipToName], " &
                       "ScheduleHistory.SoldToName = [Schedule].[SoldToName], ScheduleHistory.CustPONumber = [Schedule].[CustPONumber], " &
                       "ScheduleHistory.Revenue = [Schedule].[Revenue], ScheduleHistory.[Shipping Complete] = [Schedule].[Shipping Complete], " &
                       "ScheduleHistory.OrigShipDate = [Schedule].[OrigShipDate] " &
                        "WHERE (((Schedule.OrderKey2) Not In (SELECT OrderKey2 FROM dbo_vw_ProductionAccess)));"
        'CommandText2 = "DELETE Schedule.* FROM Schedule WHERE ((Schedule.OrderKey2) Not In (SELECT OrderKey2 FROM dbo_vw_ProductionAccess));"
        CommandText3 = "DELETE Schedule.* FROM Schedule WHERE ( ((Schedule.OrderKey2) Not In (SELECT OrderKey2 FROM dbo_vw_ProductionAccess)) AND " &
                      "( ((Schedule.OrderKey2) In (SELECT OrderKey2 FROM  ScheduleHistory)) OR " &
                      "((Schedule.OrderKey2) Not In (SELECT OrderKey2 FROM Systems)) ) );"

        'Setup dataset lock
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        InsertRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()

            'set that in transaction
            inTransBool = True

            'Execute to update present records in ScheduleHistory
            UpdateRS = ConnectionToADODB.Execute(CommandText2)

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record delete
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            InsertRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                Index = 0
                InsertRS.Open("ScheduleHistory", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that Schedule line entry
                InsertRS.AddNew()
                OrderKey = SelectRS.Fields("OrderKey2").Value
                While Index < (SelectRS.Fields.Count)
                    InsertRS.Fields.Item(SelectRS.Fields(Index).Name).Value = SelectRS.Fields(Index).Value
                    Index = Index + 1
                End While

                'Set the new record status to 9
                InsertRS.Fields.Item("OrderStatus").Value = 9

                'update recoredset and close present transacation and reinitiate a new transaction
                InsertRS.UpdateBatch()
                InsertRS.Close()

                'Insert log on which orderkeys have been moved to History
                EventLog1.WriteEntry("New OrderKey added to History: " & OrderKey & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            DeleteRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in update log string
            'EventLog1.WriteEntry("Remove old records from Schedule database", EventLogEntryType.Information)
            UpdateLog = "Clean Schedule"

        Catch ex As Exception
            'Cehck if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

        End Try

        'Everything is Done
        SelectRS = Nothing
        UpdateRS = Nothing
        InsertRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing
        Return UpdateLog

    End Function

    Private Function ScheduleQtyLogic() As String
        'Updates the part quantities for each line in an order 

        'Setup all required variables for updating the database
        Dim ConnectionToADODB As New ADODB.Connection
        Dim ConnectionString As String
        Dim CommandEncText As String
        Dim CommandQtyText As String
        Dim CommandCreatePresEx As String
        Dim CommandDeletePresEx As String
        Dim CommandExcText As String
        Dim EncQtyRS As New ADODB.Recordset
        Dim QtyRS As New ADODB.Recordset
        Dim CreateRS As New ADODB.Recordset
        Dim DropRS As New ADODB.Recordset
        Dim ExcQtyRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Initial in transaction boolean
        inTransBool = False

        'Initialze UpdateLog string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandEncText = "UPDATE Schedule SET Schedule.[Enclosure Qty] = 1 " &
                        "WHERE (((Schedule.[Enclosure Qty]) <> 1) And ((Schedule.Enclosure)= ""ENC""));"
        CommandQtyText = "UPDATE (Schedule INNER JOIN Signifiers ON (Schedule.StockCode) Like Signifiers.Signifiers+""%"") " &
                         "LEFT JOIN PresExceptions ON Schedule.OrderKey2 = PresExceptions.OrderKey2 " &
                         "SET Schedule.[OBC Qty] = [Signifiers].[OBC Qty], Schedule.[Handheld Qty] = [Signifiers].[Handheld Qty], " &
                         "Schedule.[Receiver Qty] = [Signifiers].[Receiver Qty], Schedule.[Enclosure Qty] = [Signifiers].[Enclosure Qty], " &
                         "Schedule.[Harness Qty] = [Signifiers].[Harness Qty], Schedule.[Misc Qty] = [Signifiers].[Misc Qty] " &
                         "WHERE( ((PresExceptions.OrderKey2) Is Null ) AND " &
                         "(  ((Signifiers.[Qty Exception]) = FALSE) AND ( ((Signifiers.[Handheld Qty])>0) AND ((Schedule.[Handheld Qty]) < (Signifiers.[Handheld Qty])) ) " &
                         "OR ( ((Signifiers.[Receiver Qty])>0) AND ((Schedule.[Receiver Qty]) < (Signifiers.[Receiver Qty])) ) OR " &
                         "( ((Signifiers.[Enclosure Qty])>0) AND ((Schedule.[Enclosure Qty]) < (Signifiers.[Enclosure Qty])) ) OR " &
                         "( ((Signifiers.[Harness Qty])>0) AND ((Schedule.[Harness Qty]) < (Signifiers.[Harness Qty])) ) OR " &
                         "( ((Signifiers.[Misc Qty])>0) AND ((Schedule.[Misc Qty]) < (Signifiers.[Misc Qty])) ) OR " &
                         "( ((Signifiers.[OBC Qty])>0) AND ((Schedule.[OBC Qty]) < (Signifiers.[OBC Qty])) ) ) );"
        CommandCreatePresEx = "INSERT INTO PresExceptions ( OrderKey2 ) SELECT Schedule.OrderKey2 " &
                              "FROM  (Schedule INNER JOIN Signifiers ON (Schedule.StockCode) Like Signifiers.Signifiers+""%"") " &
                              "LEFT JOIN PresExceptions ON Schedule.OrderKey2 = PresExceptions.OrderKey2 WHERE ( ((Signifiers.[Qty Exception])=True) );"
        CommandDeletePresEx = "DELETE PresExceptions.* FROM PresExceptions;"
        CommandExcText = "UPDATE Schedule, Signifiers SET Schedule.[OBC Qty] = [Signifiers].[OBC Qty], " &
                         "Schedule.[Handheld Qty] = [Signifiers].[Handheld Qty], Schedule.[Receiver Qty] = [Signifiers].[Receiver Qty], " &
                         "Schedule.[Enclosure Qty] = [Signifiers].[Enclosure Qty], Schedule.[Harness Qty] = [Signifiers].[Harness Qty], " &
                         "Schedule.[Misc Qty] = [Signifiers].[Misc Qty] WHERE (((Schedule.StockCode) Like [Signifiers].[Signifiers]+""%"") " &
                         "And ((Signifiers.[Qty Exception])=True));"

        'Setup dataset lock
        EncQtyRS.LockType = LockTypeEnum.adLockOptimistic
        ExcQtyRS.LockType = LockTypeEnum.adLockOptimistic
        QtyRS.LockType = LockTypeEnum.adLockOptimistic
        CreateRS.LockType = LockTypeEnum.adLockOptimistic
        DropRS.LockType = LockTypeEnum.adLockOptimistic


        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            EncQtyRS = ConnectionToADODB.Execute(CommandEncText)

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            ExcQtyRS = ConnectionToADODB.Execute(CommandExcText)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            DropRS = ConnectionToADODB.Execute(CommandDeletePresEx)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            CreateRS = ConnectionToADODB.Execute(CommandCreatePresEx)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            QtyRS = ConnectionToADODB.Execute(CommandQtyText)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.Close()

            'Write in log update string
            UpdateLog = "Schedule Quantities"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

        End Try

        CreateRS = Nothing
        DropRS = Nothing
        QtyRS = Nothing
        ExcQtyRS = Nothing
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function BuildQtyLogic() As String
        'Updates the part quantities for each line in the build table

        'Setup all required variables for updating the database
        Dim ConnectionToADODB As New ADODB.Connection
        Dim ConnectionString As String
        Dim CommandHHText As String
        Dim CommandQtyText As String
        Dim CommandRXText As String
        Dim HHQtyRS As New ADODB.Recordset
        Dim QtyRS As New ADODB.Recordset
        Dim RXQtyRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Initial in transaction boolean
        inTransBool = False

        'Initialze UpdateLog string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandHHText = "UPDATE (Builds INNER JOIN Schedule ON Builds.OrderKey2 = Schedule.OrderKey2) " &
                        "INNER JOIN Signifiers ON (Schedule.StockCode) Like Signifiers.Signifiers+""%"" " &
                        "SET Builds.ProductType = [Signifiers].[HH Type], Builds.[Handheld Qty] = [Signifiers].[Handheld Qty]" &
                        "WHERE ( ((Builds.[Handheld Qty])<[Signifiers].[Handheld Qty]) OR ((Builds.[Handheld Qty]) Is Null) ) " &
                        " AND ( (Builds.Cell)=""HH"" ) ;"
        CommandRXText = "UPDATE (Builds INNER JOIN Schedule ON Builds.OrderKey2 = Schedule.OrderKey2) " &
                        "INNER JOIN Signifiers ON (Schedule.StockCode) Like Signifiers.Signifiers+""%"" " &
                        "SET Builds.ProductType = [Signifiers].[RX Type], Builds.[Receiver Qty] = [Signifiers].[Receiver Qty]" &
                        "WHERE ( ((Builds.[Receiver Qty])<[Signifiers].[Receiver Qty]) OR ((Builds.[Receiver Qty]) Is Null) ) " &
                        " AND ( (Builds.Cell)=""RX"" ) ;"
        CommandQtyText = "UPDATE (Schedule INNER JOIN Builds ON Schedule.OrderKey2 = Builds.OrderKey2) " &
                         "SET Builds.OrderQty = [Schedule].[OrderQty] " &
                         "WHERE( ((Builds.OrderQty) Is Null ) OR " &
                         "(Builds.OrderQty <> [Schedule].[OrderQty]) );"

        'Setup dataset lock
        HHQtyRS.LockType = LockTypeEnum.adLockOptimistic
        RXQtyRS.LockType = LockTypeEnum.adLockOptimistic
        QtyRS.LockType = LockTypeEnum.adLockOptimistic

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            HHQtyRS = ConnectionToADODB.Execute(CommandHHText)

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            RXQtyRS = ConnectionToADODB.Execute(CommandRXText)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            QtyRS = ConnectionToADODB.Execute(CommandQtyText)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            ConnectionToADODB.Close()

            'Write in log update string
            UpdateLog = "Build Quantities"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

        End Try

        HHQtyRS = Nothing
        RXQtyRS = Nothing
        QtyRS = Nothing
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshStock() As String
        'Refreshes the "Stock" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Initial in transaction boolean
        inTransBool = False

        'Disable timer interrupt
        'Me.timer.Stop()

        'Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText3 = "UPDATE dbo_vw_ProductionAccess2 RIGHT JOIN Stock ON dbo_vw_ProductionAccess2.StockCode = Stock.StockCode SET Stock.QtyOnHand = [dbo_vw_ProductionAccess2].[QtyOnHand];"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True


            'Execute locked record update
            UpdateRS = ConnectionToADODB.Execute(CommandText3)
            'UpdateRS.Close()

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in update log string
            'EventLog1.WriteEntry("Update records the Stock database", EventLogEntryType.Information)
            UpdateLog = "Stock"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure To make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Enable timer interrupt
            'Me.timer.Start()

        End Try

        'Enable timer interrupt
        'Me.timer.Start()

        'Everything is Done
        UpdateRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshSTNStock() As String
        'Refreshes the "STN2Stock" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Initial in transaction boolean
        inTransBool = False

        'Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText3 = "UPDATE dbo_vw_ProductionAccess2 INNER JOIN STN2Stock ON dbo_vw_ProductionAccess2.StockCode = STN2Stock.StockCode " &
                       "SET STN2Stock.QtyOnHand = [dbo_vw_ProductionAccess2].[QtyOnHand], STN2Stock.QtyOnOrder = [dbo_vw_ProductionAccess2].[QtyOnOrder];"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True


            'Execute locked record update
            UpdateRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in update log string
            UpdateLog = "STN2Stock"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure To make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

        End Try

        'Everything is Done
        UpdateRS = Nothing
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshSystems() As String
        'Refreshes the "Systems" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim CommandText5 As String
        Dim CommandText6 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim SelectRS As New ADODB.Recordset
        Dim SelectRS2 As New ADODB.Recordset
        Dim DupSysRS As New ADODB.Recordset
        'Dim SysIndRS As New ADODB.Recordset
        Dim ACchkRS As New ADODB.Recordset
        Dim EXchkRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim SystemQty As Integer
        Dim Index As Integer
        Dim SysOldQty As Integer
        Dim SysNumber As Integer
        Dim SysOrderKey As String
        Dim sysATEX As Boolean
        Dim sysAC As Boolean
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Initialize the transaction boolean
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Schedule].[OrderQty], [Schedule].[OrderKey2] FROM Schedule LEFT JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2 " &
                        "WHERE (((Systems.OrderKey2) Is Null) AND (((Schedule.[Handheld Qty])>0)) OR " &
                        "(((Systems.OrderKey2) Is Null) And ((Schedule.[Receiver Qty])>0)) OR " &
                        "(((Systems.OrderKey2) Is Null) And ((Schedule.[Enclosure Qty])>0)) OR " &
                        "(((Systems.OrderKey2) Is Null) And ((Schedule.[Misc Qty])>0)) OR " &
                        "(((Systems.OrderKey2) Is Null) And ((Schedule.[OBC Qty])>0))) AND " &
                        "(Schedule.OrderQty >0) ;"

        CommandText2 = "SELECT Systems.[System Number], Systems.[System Index], Systems.[OrderKey2], Systems.ATEX, Systems.AC, Systems.[System Serial], [Schedule].[OrderQty] FROM Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2 " &
                        "WHERE (((Systems.[System Index]) Not Like [Schedule].[OrderQty]))  And (Schedule.OrderStatus Not Like ""0"");"

        'CommandText3 = "DELETE Systems.* FROM Systems WHERE (Systems.[System Index] < Systems.[System Number]); "
        CommandText3 = "DELETE Systems.* FROM (Systems LEFT JOIN Schedule ON Systems.OrderKey2 = Schedule.OrderKey2) " &
                       "LEFT JOIN ScheduleHistory ON Systems.OrderKey2 = ScheduleHistory.OrderKey2 " &
                       "WHERE (((Schedule.OrderKey2) Is Null) AND ((ScheduleHistory.OrderKey2) Is Null) OR ((Systems.[System Index]) < Systems.[System Number]));"

        CommandText5 = "UPDATE Schedule LEFT JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2 " &
                       "SET Systems.AC = True WHERE (((Systems.AC)=False) AND ((Schedule.StockCode) Like ""%-AC%""));"
        CommandText6 = "UPDATE Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2 " &
                       "SET Systems.ATEX = True WHERE (((Systems.ATEX)=False) AND ((Schedule.SoDrawingNumber) Like ""%-EX%""));"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS2.LockType = LockTypeEnum.adLockOptimistic
        DupSysRS.LockType = LockTypeEnum.adLockOptimistic
        ACchkRS.LockType = LockTypeEnum.adLockOptimistic
        EXchkRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total SystemQty & Index
        SystemQty = 0
        Index = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            'Setup the command as a prepared text command to execute more than once
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()

            'Indicate in transaction
            inTransBool = True

            'Execute select record to determine what entries to add to the Systems table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupSysRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                SystemQty = SelectRS.Fields("OrderQty").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupSysRS.Open("Systems", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that System line entry in the Order
                While (Index < SystemQty)
                    DupSysRS.AddNew()
                    DupSysRS.Fields.Item("OrderKey2").Value = SysOrderKey
                    DupSysRS.Fields.Item("System Index").Value = SystemQty
                    DupSysRS.Fields.Item("System Number").Value = Index + 1
                    DupSysRS.Fields.Item("System Serial").Value = 0
                    DupSysRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transacation and reinitiate a new transaction
                DupSysRS.Close()

                'Insert log on which orderkeys have been added to Systems
                EventLog1.WriteEntry("New OrderKey added to Systems table: " & SysOrderKey & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If


                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'check AC and ATEX products and ensure flags are set
            ACchkRS = ConnectionToADODB.Execute(CommandText5)
            EXchkRS = ConnectionToADODB.Execute(CommandText6)

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'check if the quantity has changed since original entries made
            'Initialize total SystemQty & Index
            SystemQty = 0
            Index = 0
            SysOldQty = 0
            SysNumber = 0
            sysATEX = False
            sysAC = False

            'Initiate recordset connection to the database and obtain recordset to edit
            SelectRS2.ActiveConnection = ConnectionToADODB
            UpdateRS.ActiveConnection = ConnectionToADODB
            SelectRS2.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)
            UpdateRS.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)

            ' Looping through the RecordSet if more than one record returned
            Do While (SelectRS2.EOF = False)
                SystemQty = SelectRS2.Fields("OrderQty").Value
                SysOldQty = SelectRS2.Fields("System Index").Value
                SysOrderKey = SelectRS2.Fields("OrderKey2").Value
                SysNumber = SelectRS2.Fields("System Number").Value
                Index = SysOldQty
                SelectRS2.Fields.Item("System Index").Value = SystemQty
                SelectRS2.UpdateBatch()

                ' Loop to add all necessary records for that System line entry in the Order
                If ((Index < SystemQty) And (SysNumber = Index)) Then
                    sysATEX = SelectRS2.Fields.Item("ATEX").Value
                    sysAC = SelectRS2.Fields.Item("AC").Value

                    While (Index < SystemQty)
                        UpdateRS.AddNew()
                        UpdateRS.Fields.Item("OrderKey2").Value = SysOrderKey
                        UpdateRS.Fields.Item("System Index").Value = SystemQty
                        UpdateRS.Fields.Item("System Number").Value = Index + 1
                        UpdateRS.Fields.Item("ATEX").Value = sysATEX
                        UpdateRS.Fields.Item("AC").Value = sysAC
                        UpdateRS.Fields.Item("System Serial").Value = 0
                        UpdateRS.UpdateBatch()
                        Index = Index + 1
                    End While
                End If

                'Insert log on which orderkeys have been added to Systems
                EventLog1.WriteEntry("Updated quantities for OrderKey in Systems table: " & SysOrderKey & " & SystemQty:" & SystemQty & " .", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS2.MoveNext()
            Loop

            SelectRS2.Close()
            UpdateRS.Close()

            'Commit all present transactions and start a new transaction
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Double check that now records should be removed due to quantity change
            DeleteRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "Systems"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            UpdateRS = Nothing
            SelectRS = Nothing
            SelectRS2 = Nothing
            DupSysRS = Nothing
            ACchkRS = Nothing
            EXchkRS = Nothing
            DeleteRS = Nothing
            'ConnectionToADODB.Dispose()
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        UpdateRS = Nothing
        SelectRS = Nothing
        SelectRS2 = Nothing
        DupSysRS = Nothing
        ACchkRS = Nothing
        EXchkRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshHandhelds() As String
        'Refreshes the "Handhelds" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText5 As String
        Dim CommandText6 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim SelectRS As New ADODB.Recordset
        Dim SelectRS2 As New ADODB.Recordset
        Dim DupHHRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim HHQty As Integer
        Dim SysID As Integer
        Dim oldHHQty As Integer
        Dim HHNumber As Integer
        Dim Index As Integer
        Dim UpdateLog As String
        Dim inTransBool As Boolean
        Dim SysOrderKey As String

        'Initalize the transaction status
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Systems].[System_ID],[Schedule].[Handheld Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) LEFT JOIN Handhelds ON " &
                      " Systems.System_ID = Handhelds.System_ID WHERE (((Handhelds.System_ID) Is Null) AND ((Schedule.[Handheld Qty])>0)); "

        CommandText5 = "DELETE Handhelds.* FROM Systems RIGHT JOIN Handhelds ON Systems.System_ID = Handhelds.System_ID  WHERE " &
                        "(((Systems.System_ID) Is Null) OR (Handhelds.[Handheld Index] < Handhelds.[Handheld Number])); "

        CommandText6 = "SELECT Handhelds.[Handheld Number], Handhelds.[Handheld Index], Handhelds.[System_ID], [Schedule].[Handheld Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) INNER JOIN Handhelds ON " &
                        "Systems.System_ID = Handhelds.System_ID WHERE (((Handhelds.[Handheld Index]) Not Like [Schedule].[Handheld Qty]));"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        DupHHRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total Handheld Qty & Index
        HHQty = 0
        Index = 0
        SysID = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Handhelds table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupHHRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                HHQty = SelectRS.Fields("Handheld Qty").Value
                SysID = SelectRS.Fields("System_ID").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupHHRS.Open("Handhelds", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that System line entry in the Order
                While (Index < HHQty)
                    DupHHRS.AddNew()
                    DupHHRS.Fields.Item("System_ID").Value = SysID
                    DupHHRS.Fields.Item("Handheld Index").Value = HHQty
                    DupHHRS.Fields.Item("Handheld Number").Value = Index + 1
                    DupHHRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transacation and reinitiate a new transaction
                DupHHRS.Close()

                'Insert log on which orderkeys have been added to Handheld
                EventLog1.WriteEntry("New OrderKey added to Handheld table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Find any records that need to be updated with new handheld quantity
            'Initiate recordset connection to the database and obtain recordset to edit
            SelectRS2.ActiveConnection = ConnectionToADODB
            UpdateRS.ActiveConnection = ConnectionToADODB
            SelectRS2.Open(CommandText6, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)
            UpdateRS.Open(CommandText6, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)

            ' Looping through the RecordSet if more than one record returned
            Do While (SelectRS2.EOF = False)
                HHQty = SelectRS2.Fields("Handheld Qty").Value
                oldHHQty = SelectRS2.Fields("Handheld Index").Value
                HHNumber = SelectRS2.Fields("Handheld Number").Value
                SysID = SelectRS2.Fields("System_ID").Value
                SysOrderKey = SelectRS2.Fields("OrderKey2").Value
                Index = oldHHQty
                SelectRS2.Fields.Item("Handheld Index").Value = HHQty
                SelectRS2.UpdateBatch()

                ' Loop to add all necessary records for that Handheld line entry in the Order
                If ((Index < HHQty) And (HHNumber = Index)) Then

                    While (Index < HHQty)
                        UpdateRS.AddNew()
                        UpdateRS.Fields.Item("System_ID").Value = SysID
                        UpdateRS.Fields.Item("Handheld Index").Value = HHQty
                        UpdateRS.Fields.Item("Handheld Number").Value = Index + 1
                        UpdateRS.UpdateBatch()
                        Index = Index + 1
                    End While
                End If

                'Insert log on which orderkeys have been added to Handheld
                EventLog1.WriteEntry("Update quantities to OrderKey in Handheld table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS2.MoveNext()
            Loop

            SelectRS2.Close()
            UpdateRS.Close()

            'Commit all present transactions and start a new transaction
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            DeleteRS = ConnectionToADODB.Execute(CommandText5)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "Handhelds"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            UpdateRS = Nothing
            SelectRS = Nothing
            SelectRS2 = Nothing
            DupHHRS = Nothing
            DeleteRS = Nothing
            'ConnectionToADODB.Dispose()
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        UpdateRS = Nothing
        SelectRS = Nothing
        SelectRS2 = Nothing
        DupHHRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        'restart timer
        'Me.timer.Start()

        Return UpdateLog

    End Function

    Private Function RefreshReceivers() As String
        'Refreshes the "Receivers" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim SelectRS As New ADODB.Recordset
        Dim SelectRS2 As New ADODB.Recordset
        Dim DupRXRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim RXQty As Integer
        Dim SysID As Integer
        Dim oldRXQty As Integer
        Dim RXNumber As Integer
        Dim Index As Integer
        Dim UpdateLog As String
        Dim inTransBool As Boolean
        Dim SysOrderKey As String

        'Initialize the transaction indicator
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Systems].[System_ID], [Schedule].[Receiver Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) LEFT JOIN Receivers ON " &
                      "Systems.System_ID = Receivers.System_ID WHERE (((Receivers.System_ID) Is Null) AND ((Schedule.[Receiver Qty])>0)); "

        CommandText2 = "SELECT Receivers.[Receiver Number], Receivers.[Receiver Index], Receivers.System_ID, [Schedule].[Receiver Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON " &
                        "Schedule.OrderKey2 = Systems.OrderKey2) INNER JOIN Receivers ON Systems.System_ID = Receivers.System_ID WHERE " &
                        "(Receivers.[Receiver Index] Not Like [Schedule].[Receiver Qty]);"

        CommandText3 = "DELETE Receivers.* FROM Systems RIGHT JOIN Receivers ON Systems.System_ID = Receivers.System_ID WHERE " &
                        "((Systems.System_ID Is Null) OR (Receivers.[Receiver Index] < Receivers.[Receiver Number]));"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS2.LockType = LockTypeEnum.adLockOptimistic
        DupRXRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total Receiver Qty & Index
        RXQty = 0
        Index = 0
        SysID = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Receivers table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupRXRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                RXQty = SelectRS.Fields("Receiver Qty").Value
                SysID = SelectRS.Fields("System_ID").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupRXRS.Open("Receivers", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that receiver line entry in the Order
                While (Index < RXQty)
                    DupRXRS.AddNew()
                    DupRXRS.Fields.Item("System_ID").Value = SysID
                    DupRXRS.Fields.Item("Receiver Index").Value = RXQty
                    DupRXRS.Fields.Item("Receiver Number").Value = Index + 1
                    DupRXRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transaction and reinitiate a new transaction in next loop
                DupRXRS.Close()

                'Insert log on which orderkeys have been added to Receivers
                EventLog1.WriteEntry("New OrderKey added to Receivers table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Find any records that need to be updated with new receiver quantity
            'Initiate recordset connection to the database and obtain recordset to edit
            SelectRS2.ActiveConnection = ConnectionToADODB
            UpdateRS.ActiveConnection = ConnectionToADODB
            SelectRS2.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)
            UpdateRS.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)

            'Looping through the recordset if more than one record returned
            Do While (SelectRS2.EOF = False)
                RXQty = SelectRS2.Fields("Receiver Qty").Value
                oldRXQty = SelectRS2.Fields("Receiver Index").Value
                RXNumber = SelectRS2.Fields("Receiver Number").Value
                SysID = SelectRS2.Fields("System_ID").Value
                SysOrderKey = SelectRS2.Fields("OrderKey2").Value
                Index = oldRXQty
                SelectRS2.Fields.Item("Receiver Index").Value = RXQty
                SelectRS2.UpdateBatch()

                'Loop to add all necessary records for that System line entry in the order
                If ((Index < RXQty) And (RXNumber = Index)) Then
                    While (Index < RXQty)
                        UpdateRS.AddNew()
                        UpdateRS.Fields.Item("System_ID").Value = SysID
                        UpdateRS.Fields.Item("Receiver Index").Value = RXQty
                        UpdateRS.Fields.Item("Receiver Number").Value = Index + 1
                        UpdateRS.UpdateBatch()
                        Index = Index + 1
                    End While
                End If

                'Insert log on which orderkeys have been added to Receivers
                EventLog1.WriteEntry("Update quantities for OrderKey in Receivers table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS2.MoveNext()
            Loop

            SelectRS2.Close()
            UpdateRS.Close()

            'Commit all present transactions and start a new transaction
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            DeleteRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "Receivers"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            UpdateRS = Nothing
            SelectRS = Nothing
            SelectRS2 = Nothing
            DupRXRS = Nothing
            DeleteRS = Nothing
            'ConnectionToADODB.Dispose()
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        UpdateRS = Nothing
        SelectRS = Nothing
        SelectRS2 = Nothing
        DupRXRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshEnclosures() As String
        'Refreshes the "Enclosures" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim SelectRS As New ADODB.Recordset
        Dim SelectRS2 As New ADODB.Recordset
        Dim DupEncRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim EncQty As Integer
        Dim SysID As Integer
        Dim oldEncQty As Integer
        Dim EncNumber As Integer
        Dim Index As Integer
        Dim UpdateLog As String
        Dim inTransBool As Boolean
        Dim SysOrderKey As String

        'Initialize transaction indicator
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Systems].[System_ID], [Schedule].[Enclosure Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) LEFT JOIN Enclosures ON " &
                      "Systems.System_ID = Enclosures.System_ID WHERE (((Enclosures.System_ID) Is Null) AND ((Schedule.[Enclosure Qty])>0)); "

        CommandText2 = "SELECT Enclosures.[Enclosure Number], Enclosures.[Enclosure Index], Enclosures.[System_ID], [Schedule].[Enclosure Qty], [Systems].[OrderKey2] FROM " &
                        "(Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) INNER JOIN Enclosures ON Systems.System_ID = Enclosures.System_ID " &
                       "WHERE (((Enclosures.[Enclosure Index]) Not Like [Schedule].[Enclosure Qty]));"

        CommandText3 = "DELETE Enclosures.* FROM Systems RIGHT JOIN Enclosures ON Systems.System_ID = Enclosures.System_ID WHERE " &
                        "((Systems.System_ID Is Null) OR (Enclosures.[Enclosure Index] < Enclosures.[Enclosure Number]));"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS2.LockType = LockTypeEnum.adLockOptimistic
        DupEncRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total Enclosure Qty & Index
        EncQty = 0
        Index = 0
        SysID = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Enclosures table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupEncRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                EncQty = SelectRS.Fields("Enclosure Qty").Value
                SysID = SelectRS.Fields("System_ID").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupEncRS.Open("Enclosures", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that System line entry in the Order
                While (Index < EncQty)
                    DupEncRS.AddNew()
                    DupEncRS.Fields.Item("System_ID").Value = SysID
                    DupEncRS.Fields.Item("Enclosure Index").Value = EncQty
                    DupEncRS.Fields.Item("Enclosure Number").Value = Index + 1
                    DupEncRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transaction and reintiate a new transaction next loop
                DupEncRS.Close()

                'Insert log on which orderkeys have been added to Enclosures
                EventLog1.WriteEntry("New OrderKey added to Enclosures table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Find any records that need to be updated with new enclosure quantity 
            'Initiate recordset connection to the database and obtain recordset to edit
            SelectRS2.ActiveConnection = ConnectionToADODB
            UpdateRS.ActiveConnection = ConnectionToADODB
            SelectRS2.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)
            UpdateRS.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)

            'Looping through the Recordset if more than one record returned
            Do While (SelectRS2.EOF = False)
                EncQty = SelectRS2.Fields("Enclosure Qty").Value
                oldEncQty = SelectRS2.Fields("Enclosure Index").Value
                EncNumber = SelectRS2.Fields("Enclosure Number").Value
                SysID = SelectRS2.Fields("System_ID").Value
                SysOrderKey = SelectRS2.Fields("OrderKey2").Value
                Index = oldEncQty
                SelectRS2.Fields.Item("Enclosure Index").Value = EncQty
                SelectRS2.UpdateBatch()

                'Loop to add all necessary records for that System line entry in the order
                If ((Index < EncQty) And (EncNumber = Index)) Then
                    While (Index < EncQty)
                        UpdateRS.AddNew()
                        UpdateRS.Fields.Item("System_ID").Value = SysID
                        UpdateRS.Fields.Item("Enclosure Index").Value = EncQty
                        UpdateRS.Fields.Item("Enclosure Number").Value = Index + 1
                        UpdateRS.UpdateBatch()
                        Index = Index + 1
                    End While
                End If

                'Insert log on which orderkeys have been added to Enclosures
                EventLog1.WriteEntry("Update quantities for OrderKey in the Enclosures table: " & SysOrderKey & " & SysID: " & SysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS2.MoveNext()
            Loop

            SelectRS2.Close()
            UpdateRS.Close()

            'Commit all present transactions and start a new transaction
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            DeleteRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "Enclosures"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            UpdateRS = Nothing
            SelectRS = Nothing
            SelectRS2 = Nothing
            DupEncRS = Nothing
            DeleteRS = Nothing
            'ConnectionToADODB.Dispose()
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        UpdateRS = Nothing
        SelectRS = Nothing
        SelectRS2 = Nothing
        DupEncRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshMiscs() As String
        'Refreshes the "Miscs" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim UpdateRS As New ADODB.Recordset
        Dim SelectRS As New ADODB.Recordset
        Dim SelectRS2 As New ADODB.Recordset
        Dim DupMiscRS As New ADODB.Recordset
        Dim DeleteRS As New ADODB.Recordset
        Dim MiscQty As Integer
        Dim sysID As Integer
        Dim oldMiscQty As Integer
        Dim MiscNumber As Integer
        Dim Index As Integer
        Dim UpdateLog As String
        Dim inTransBool As Boolean
        Dim SysOrderKey As String

        'Intialize transaction indicator
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Systems].[System_ID], [Schedule].[Misc Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) LEFT JOIN Misc ON Systems.System_ID = Misc.System_ID " &
                      "WHERE (((Misc.System_ID) Is Null) AND ((Schedule.[Misc Qty])>0));"

        CommandText2 = "SELECT Misc.[Misc Number], Misc.[Misc Index], Misc.[System_ID], [Schedule].[Misc Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) " &
                       "INNER JOIN Misc ON Systems.System_ID = Misc.System_ID WHERE (((Misc.[Misc Index]) Not Like [Schedule].[Misc Qty]));"

        CommandText3 = "DELETE Misc.* FROM Systems RIGHT JOIN Misc ON Systems.System_ID = Misc.System_ID WHERE " &
                       "(((Systems.System_ID) Is Null) OR (Misc.[Misc Index] < Misc.[Misc Number]));"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS2.LockType = LockTypeEnum.adLockOptimistic
        DupMiscRS.LockType = LockTypeEnum.adLockOptimistic
        DeleteRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total Misc Qty & Index
        MiscQty = 0
        Index = 0
        sysID = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Misc table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupMiscRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                MiscQty = SelectRS.Fields("Misc Qty").Value
                sysID = SelectRS.Fields("System_ID").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupMiscRS.Open("Misc", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that System line entry in the Order
                While (Index < MiscQty)
                    DupMiscRS.AddNew()
                    DupMiscRS.Fields.Item("System_ID").Value = sysID
                    DupMiscRS.Fields.Item("Misc Index").Value = MiscQty
                    DupMiscRS.Fields.Item("Misc Number").Value = Index + 1
                    DupMiscRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transaction and reintiate a new transaction next loop
                DupMiscRS.Close()

                'Insert log on which orderkeys have been added to Miscs
                EventLog1.WriteEntry("New OrderKey added to Miscs table: " & SysOrderKey & " & SysID: " & sysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Find any records that need to be updated with new misc quantity 
            'Initiate recordset connection to the database and obtain recordset to edit
            SelectRS2.ActiveConnection = ConnectionToADODB
            UpdateRS.ActiveConnection = ConnectionToADODB
            SelectRS2.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)
            UpdateRS.Open(CommandText2, , CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockBatchOptimistic)

            'Looping through the Recordset if more than one record returned
            Do While (SelectRS2.EOF = False)
                MiscQty = SelectRS2.Fields("Misc Qty").Value
                oldMiscQty = SelectRS2.Fields("Misc Index").Value
                MiscNumber = SelectRS2.Fields("Misc Number").Value
                sysID = SelectRS2.Fields("System_ID").Value
                SysOrderKey = SelectRS2.Fields("OrderKey2").Value
                Index = oldMiscQty
                SelectRS2.Fields.Item("Misc Index").Value = MiscQty
                SelectRS2.UpdateBatch()

                'Loop to add all necessary records for that System line entry in the order
                If ((Index < MiscQty) And (MiscNumber = Index)) Then
                    While (Index < MiscQty)
                        UpdateRS.AddNew()
                        UpdateRS.Fields.Item("System_ID").Value = sysID
                        UpdateRS.Fields.Item("Misc Index").Value = MiscQty
                        UpdateRS.Fields.Item("Misc Number").Value = Index + 1
                        UpdateRS.UpdateBatch()
                        Index = Index + 1
                    End While
                End If

                'Insert log on which orderkeys have been added to Miscs
                EventLog1.WriteEntry("Update quantities for OrderKey in Miscs table: " & SysOrderKey & " & SysID: " & sysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS2.MoveNext()
            Loop

            SelectRS2.Close()
            UpdateRS.Close()

            'Commit all present transactions and start a new transaction
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            DeleteRS = ConnectionToADODB.Execute(CommandText3)

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "Miscs"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            UpdateRS = Nothing
            SelectRS = Nothing
            SelectRS2 = Nothing
            DupMiscRS = Nothing
            DeleteRS = Nothing
            'ConnectionToADODB.Dispose()
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        UpdateRS = Nothing
        SelectRS = Nothing
        SelectRS2 = Nothing
        DupMiscRS = Nothing
        DeleteRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshOBCs() As String
        'Refreshes the "OBCs" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim ConnectionString As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim SelectRS As New ADODB.Recordset
        Dim DupOBCRS As New ADODB.Recordset
        Dim OBCQty As Integer
        Dim sysID As Integer
        Dim Index As Integer
        Dim UpdateLog As String
        Dim inTransBool As Boolean
        Dim SysOrderKey As String

        'Initialize transaction indicator
        inTransBool = False

        ' Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT [Systems].[System_ID], [Schedule].[OBC Qty], [Systems].[OrderKey2] FROM (Schedule INNER JOIN Systems ON Schedule.OrderKey2 = Systems.OrderKey2) LEFT JOIN OBCs ON Systems.System_ID = OBCs.System_ID " &
                      "WHERE (((OBCs.System_ID) Is Null) AND ((Schedule.[OBC Qty])>0));"

        'Setup dataset lock
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        DupOBCRS.LockType = LockTypeEnum.adLockOptimistic

        'Initialize total Misc Qty & Index
        OBCQty = 0
        Index = 0
        sysID = 0

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Misc table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            'Initiate recordset connection to the database
            DupOBCRS.ActiveConnection = ConnectionToADODB

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                OBCQty = SelectRS.Fields("OBC Qty").Value
                sysID = SelectRS.Fields("System_ID").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                Index = 0
                DupOBCRS.Open("OBCs", , , LockTypeEnum.adLockBatchOptimistic)

                ' Loop to add all necessary records for that System line entry in the Order
                While (Index < OBCQty)
                    DupOBCRS.AddNew()
                    DupOBCRS.Fields.Item("System_ID").Value = sysID
                    DupOBCRS.Fields.Item("OBC Serial").Value = 0
                    DupOBCRS.UpdateBatch()
                    Index = Index + 1
                End While

                'Close present transaction and reintiate a new transaction next loop
                DupOBCRS.Close()

                'Insert log on which orderkeys have been added to OBCs
                EventLog1.WriteEntry("New OrderKey for OBCs table: " & SysOrderKey & " & SysID: " & sysID & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write in Update log string
            UpdateLog = "OBCs"

        Catch ex As Exception
            'Check if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Everything is Done
            SelectRS = Nothing
            ConnectionToADODB = Nothing

        End Try

        'Everything is Done
        SelectRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        Return UpdateLog

    End Function

    Private Function RefreshSchedule() As String
        'Refreshes the "Schedule" Access database using OleDb commands to update new lines

        'Setup all required variables for updating the database
        Dim CommandText As String
        Dim CommandText2 As String
        Dim CommandText3 As String
        Dim ConnectionString As String
        Dim SysOrderKey As String
        Dim SysOrderQty As String
        Dim SysStockCode As String
        Dim SysOrderStatus As String
        Dim ConnectionToADODB As New ADODB.Connection
        Dim SelectRS As New ADODB.Recordset
        Dim InsertRS As New ADODB.Recordset
        Dim UpdateRS As New ADODB.Recordset
        Dim UpdateLog As String
        Dim inTransBool As Boolean

        'Disable timer interrupt
        'Me.timer.Stop()

        'Initialize the update log string
        UpdateLog = ""

        'create strings for OleDB connection and SQL command to update the database
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & Me.DatabasePath & "\Production Schedule.accdb"
        CommandText = "SELECT dbo_vw_ProductionAccess.OrderKey2,dbo_vw_ProductionAccess.StockCode,dbo_vw_ProductionAccess.SoDrawingNumber,dbo_vw_ProductionAccess.OrderQty, dbo_vw_ProductionAccess.OrderStatus " &
                    "FROM dbo_vw_ProductionAccess LEFT JOIN Schedule On dbo_vw_ProductionAccess.OrderKey2 = Schedule.OrderKey2 " &
                    "WHERE ((((Schedule.OrigShipDate) > dbo_vw_ProductionAccess.ReqShipDate))" &
                    "Or (((Schedule.ReqShipDate) > dbo_vw_ProductionAccess.ReqShipDate1))" &
                    "Or (((Schedule.StockCode) Not Like dbo_vw_ProductionAccess.StockCode))" &
                    "Or (((Schedule.SoDrawingNumber) Not Like dbo_vw_ProductionAccess.SoDrawingNumber))" &
                    "Or (((Schedule.OrderQty) Not Like dbo_vw_ProductionAccess.OrderQty))" &
                    "Or (((Schedule.OrderStatus) Not Like dbo_vw_ProductionAccess.OrderStatus))" &
                    "Or (((Schedule.StrictShipDate) Not Like dbo_vw_ProductionAccess.SOSHIP))" &
                    "Or (((Schedule.SalesOrderLine) Not Like dbo_vw_ProductionAccess.[SalesOrderLine]))" &
                    "Or (((Schedule.CommentsOnOneLine) Is Not Null))" &
                    "Or (((dbo_vw_ProductionAccess.CommentsOnOneLine) Is Not Null))" &
                    "Or ((Schedule.ReqShipDate) Is Null)" &
                    "Or ((Schedule.SoDrawingNumber) Is Null)" &
                    "Or ((Schedule.ShipToName) Is Null)" &
                    "Or ((Schedule.SoldToName) Is Null)" &
                    "Or (((Schedule.ShipToName) Not Like dbo_vw_ProductionAccess.[ShipToName]))" &
                    "Or (((Schedule.SoldToName) Not Like dbo_vw_ProductionAccess.[SoldToName2])) " &
                    "Or (((Schedule.Revenue)<FormatCurrency((dbo_vw_ProductionAccess.PriceExtendedCdn),4)))  " &
                    "Or ((Schedule.Revenue) Is Null)) AND " &
                    "((dbo_vw_ProductionAccess.OrderQty) >= 0) AND " &
                    "((Schedule.OrderKey2) Is Null) AND" &
                    "((dbo_vw_ProductionAccess.OrderStatus) Not Like ""0"");"
        CommandText2 = "INSERT INTO Schedule ( OrderKey2, ReqShipDate, SalesOrder, SalesOrderLine, StockCode, SoDrawingNumber, OrderQty, OrderStatus, " &
                    "StrictShipDate, Enclosure, SalesOrderInitLine, CommentsOnOneLine, Country, ShipToName, SoldToName, CustPONumber, Revenue, OrigShipDate ) " &
                    "Select dbo_vw_ProductionAccess.OrderKey2, dbo_vw_ProductionAccess.ReqShipDate1, " &
                    "dbo_vw_ProductionAccess.SalesOrder, dbo_vw_ProductionAccess.SalesOrderLine, " &
                    "dbo_vw_ProductionAccess.StockCode, dbo_vw_ProductionAccess.SoDrawingNumber, dbo_vw_ProductionAccess.OrderQty, dbo_vw_ProductionAccess.OrderStatus, " &
                    "dbo_vw_ProductionAccess.SOSHIP, dbo_vw_ProductionAccess.Enclosure, dbo_vw_ProductionAccess.SalesOrderInitLine, dbo_vw_ProductionAccess.CommentsOnOneLine, " &
                    "dbo_vw_ProductionAccess.ShipAddress5, dbo_vw_ProductionAccess.ShipToName, dbo_vw_ProductionAccess.SoldToName2, dbo_vw_ProductionAccess.CustomerPoNumber, " &
                    "dbo_vw_ProductionAccess.PriceExtendedCdn, dbo_vw_ProductionAccess.ReqShipDate " &
                    "From dbo_vw_ProductionAccess LEFT Join Schedule On dbo_vw_ProductionAccess.OrderKey2 = Schedule.OrderKey2 " &
                    "WHERE ((dbo_vw_ProductionAccess.OrderQty) >= 0) AND " &
                    "((Schedule.OrderKey2) Is Null) AND" &
                    "((dbo_vw_ProductionAccess.OrderStatus) Not Like ""0"");"
        CommandText3 = "UPDATE Schedule INNER JOIN dbo_vw_ProductionAccess On dbo_vw_ProductionAccess.OrderKey2 = Schedule.OrderKey2 " &
                        "Set Schedule.ReqShipDate = dbo_vw_ProductionAccess!ReqShipDate1," &
                        "Schedule.SalesOrder = [dbo_vw_ProductionAccess].[SalesOrder]," &
                        "Schedule.Country = [dbo_vw_ProductionAccess].[ShipAddress5]," &
                        "Schedule.SalesOrderInitLine = [dbo_vw_ProductionAccess].[SalesOrderInitLine]," &
                        "Schedule.StockCode = [dbo_vw_ProductionAccess].[StockCode]," &
                        "Schedule.SoDrawingNumber = [dbo_vw_ProductionAccess].[SoDrawingNumber]," &
                        "Schedule.OrderQty = [dbo_vw_ProductionAccess].[OrderQty]," &
                        "Schedule.OrderStatus = [dbo_vw_ProductionAccess].[OrderStatus]," &
                        "Schedule.OrderKey2 = [dbo_vw_ProductionAccess].[OrderKey2]," &
                        "Schedule.SalesOrderLine = [dbo_vw_ProductionAccess].[SalesOrderLine]," &
                        "Schedule.StrictShipDate = [dbo_vw_ProductionAccess].[SOSHIP]," &
                        "Schedule.Enclosure = [dbo_vw_ProductionAccess].[Enclosure]," &
                        "Schedule.CommentsOnOneLine = [dbo_vw_ProductionAccess].[CommentsOnOneLine]," &
                        "Schedule.ShipToName = [dbo_vw_ProductionAccess].[ShipToName]," &
                        "Schedule.SoldToName = [dbo_vw_ProductionAccess].[SoldToName2]," &
                        "Schedule.CustPONumber = [dbo_vw_ProductionAccess].[CustomerPoNumber]," &
                        "Schedule.Revenue = [dbo_vw_ProductionAccess].[PriceExtendedCdn], " &
                        "Schedule.OrigShipDate = [dbo_vw_ProductionAccess].[ReqShipDate] " &
                        "WHERE ((((Schedule.ReqShipDate) Not Like dbo_vw_ProductionAccess.ReqShipDate1))" &
                        "Or ( (((Schedule.StockCode) Not Like dbo_vw_ProductionAccess.StockCode)) )" &
                        "Or (((Schedule.SoDrawingNumber) Not Like dbo_vw_ProductionAccess.SoDrawingNumber))" &
                        "Or (((Schedule.OrderQty) Not Like dbo_vw_ProductionAccess.OrderQty))" &
                        "Or (((Schedule.OrderStatus) Not Like dbo_vw_ProductionAccess.OrderStatus))" &
                        "Or (((Schedule.StrictShipDate) Not Like dbo_vw_ProductionAccess.SOSHIP))" &
                        "Or (((Schedule.SalesOrderLine) Not Like dbo_vw_ProductionAccess.[SalesOrderLine]))" &
                        "Or (((Schedule.CommentsOnOneLine) Not Like dbo_vw_ProductionAccess.CommentsOnOneLine))" &
                        "Or ((Schedule.ReqShipDate) Is Null)" &
                        "Or ((Schedule.OrigShipDate) Is Null)" &
                        "Or ((Schedule.ShipToName) Is Null)" &
                        "Or ((Schedule.SoldToName) Is Null)" &
                        "Or ((Schedule.Revenue) Is Null)" &
                        "Or ((Schedule.SoDrawingNumber) Is Null)" &
                        "Or (((Schedule.ShipToName) Not Like dbo_vw_ProductionAccess.[ShipToName]))" &
                        "Or (((Schedule.SoldToName) Not Like dbo_vw_ProductionAccess.[SoldToName2]))" &
                        "Or (((Schedule.CustPONumber) Not Like dbo_vw_ProductionAccess.[CustomerPoNumber]))" &
                        "Or (((Schedule.Revenue)<FormatCurrency((dbo_vw_ProductionAccess.PriceExtendedCdn),4))) " &
                        "Or (((Schedule.OrigShipDate) Not Like dbo_vw_ProductionAccess.ReqShipDate)) ) AND " &
                        "((Schedule.OrderKey2) Is Not Null);"

        'Setup dataset lock
        UpdateRS.LockType = LockTypeEnum.adLockOptimistic
        SelectRS.LockType = LockTypeEnum.adLockOptimistic
        InsertRS.LockType = LockTypeEnum.adLockOptimistic

        'Attempt to open connection and complete a transaction
        Try
            'Open and begin transaction
            ConnectionToADODB.Open(ConnectionString)
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute select record to determine what entries to add to the Schedule table
            SelectRS = ConnectionToADODB.Execute(CommandText)

            ' Looping through the RecordSet if more than one record returned
            Do While SelectRS.EOF = False
                SysOrderQty = SelectRS.Fields("OrderQty").Value
                SysOrderKey = SelectRS.Fields("OrderKey2").Value
                SysOrderStatus = SelectRS.Fields("OrderStatus").Value
                SysStockCode = SelectRS.Fields("StockCode").Value

                'Insert log on which orderkeys have been added to Systems
                EventLog1.WriteEntry("New OrderKey added to Schedule table: " & SysOrderKey & ", StockCode:" & SysStockCode & ", Status:" & SysOrderStatus & ".", EventLogEntryType.Information, eventId2)
                If (eventId2 = 65535) Then
                    eventId2 = 1
                Else
                    eventId2 = eventId2 + 1
                End If

                SelectRS.MoveNext()
            Loop

            'close and restart transaction to unlock records
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execut the record creation
            InsertRS = ConnectionToADODB.Execute(CommandText2)
            ConnectionToADODB.CommitTrans()
            inTransBool = False

            'Start new transaction
            ConnectionToADODB.BeginTrans()
            inTransBool = True

            'Execute locked record update
            UpdateRS = ConnectionToADODB.Execute(CommandText3)
            'UpdateRS.Close()

            'Commit transaction and close connection
            ConnectionToADODB.CommitTrans()
            inTransBool = False
            ConnectionToADODB.Close()

            'Write event in log
            'EventLog1.WriteEntry("Updating the Schedule database", EventLogEntryType.Information)
            UpdateLog = "Schedule"

        Catch ex As Exception
            'Cehck if in transaction and Rollback transaction if an issue arises
            If (inTransBool) Then
                ConnectionToADODB.RollbackTrans()
            End If

            'Write error event to log file.
            EventLog1.WriteEntry("Failure to make database connection with following error: " & ex.Message & "stack trace: " & ex.StackTrace & " trans was rolled back:" & inTransBool, EventLogEntryType.Error, eventId)
            If (eventId = 65535) Then
                eventId = 1
            Else
                eventId = eventId + 1
            End If

            'Enable timer interrupt
            'Me.timer.Start()

        End Try

        'Everything is Done
        UpdateRS = Nothing
        'ConnectionToADODB.Dispose()
        ConnectionToADODB = Nothing

        'Enable timer interrupt
        'Me.timer.Start()

        Return UpdateLog

    End Function

    'Friend timer As Timer
End Class
