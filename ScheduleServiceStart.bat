@ECHO OFF
timeout /t 60 >nul
echo %date% %time% - Database Service Start Initiated >>"D:\ProductionScheduleUpdate\ScheduleServiceStart.log"
sc START ScheduleUpdateNew "\\ENGINEERING-SRV.corp.baseng.com\Data\Production\Schedule" >>"D:\ProductionScheduleUpdate\ScheduleServiceStart.log"
echo %date% %time% - Database Service Started Successfully >>"D:\ProductionScheduleUpdate\ScheduleServiceStart.log"
echo   >>"D:\ProductionScheduleUpdate\ScheduleServiceStart.log"