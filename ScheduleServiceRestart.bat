@ECHO OFF
echo %date% %time% - Database Service Restart Initiated >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
sc QUERY ScheduleUpdateNew >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"

for /F "tokens=3 delims=: " %%H in ('sc query ScheduleUpdateNew ^| findstr "        STATE"') do (
	if /I "%%H" NEQ "RUNNING" (
		sc START ScheduleUpdateNew "\\ENGINEERING-SRV.corp.baseng.com\Data\Production\Schedule" >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		echo %date% %time% - Database Service Started Successfully >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		goto end
	)
	else if /I "%%H" EQU "RUNNING" (
		sc STOP ScheduleUpdateNew >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		sc START ScheduleUpdateNew "\\ENGINEERING-SRV.corp.baseng.com\Data\Production\Schedule" >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		echo %date% %time% - Database Service Restarted Successfully >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		goto end
	)
)
:end
echo   >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
exit