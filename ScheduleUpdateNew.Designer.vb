﻿Imports System.Timers
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.IO

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ScheduleUpdateNew
    Inherits System.ServiceProcess.ServiceBase

    'UserService overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            EventLog1.Dispose()
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' The main entry point for the process
    <MTAThread()>
    <System.Diagnostics.DebuggerNonUserCode()>
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        'ServicesToRun = New System.ServiceProcess.ServiceBase() {New ScheduleUpdate(cmdArgs)}
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ScheduleUpdateNew()}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    Public Sub New()
        MyBase.New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'Add an Event Logger for the service
        Me.EventLog1 = New System.Diagnostics.EventLog
        If Not System.Diagnostics.EventLog.SourceExists("MyNewSource") Then
            System.Diagnostics.EventLog.CreateEventSource("MyNewSource", "ProdUpdateLog")
        End If

        EventLog1.Source = "MyNewSource"
        EventLog1.Log = "ProdUpdateLog"

        'Setup string for path of database
        'Me.DatabasePath = cmdArgs(0)

    End Sub

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.EventLog1 = New System.Diagnostics.EventLog()
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'ScheduleUpdate
        '
        Me.ServiceName = "ProductionScheduleUpdateNew"
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents EventLog1 As EventLog
    Friend DatabasePath As String
    Friend Watcher As FileSystemWatcher
    Friend timer As Timer
End Class
