@ECHO Off 

::Setup variables to be used to check if  database is being updated properly
SET PresDate=%date%
Set PresTime=%time%
Set PowerShellDir=C:\Windows\System32\WindowsPowerShell\v1.0

setlocal EnableDelayedExpansion

::Check if service is running
echo %date% %time% - Database Service Check Initiated >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
sc QUERY ScheduleUpdateNew >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"

for /F "tokens=3 delims=: " %%H in ('sc query ScheduleUpdateNew ^| findstr "        STATE"') do (
	if /I "%%H" NEQ "RUNNING" (
		sc START ScheduleUpdateNew "\\ENGINEERING-SRV.corp.baseng.com\Data\Production\Schedule" >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		echo %date% %time% - Database Service Started Successfully >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
		
		::Send email indicating issue
		CD /D "%PowerShellDir%"
		Powershell -ExecutionPolicy Bypass -Command "& 'D:\ProductionScheduleUpdate\FAILUREEMAIL.ps1' '%PresDate%' '%PresTime%'"
		
		goto end
	)
	else if /I "%%H" EQU "RUNNING" (
		goto end
	)
)
:end
echo   >>"D:\ProductionScheduleUpdate\ScheduleServiceRestart.log"
exit

	
	
