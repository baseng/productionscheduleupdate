Set appAccess = CreateObject("Access.Application")
strDBNameAndPath = "\\DC1\Data\Production\Schedule\Production Schedule.accdb"

appAccess.Visible = True

appAccess.OpenCurrentDatabase strDBNameAndPath

appAccess.DoCmd.RunMacro "VerifyOrderKey"

appAccess.CloseCurrentDatabase

Set appAccess = Nothing